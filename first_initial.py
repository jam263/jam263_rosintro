# Jacob Manders HW 6 P3

#!/usr/bin/env python

# Python 2/3 compatibility imports - copying imports from the tutorial
from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node("homework_6_p3", anonymous=True)

# Instantiate the robot and the scene
robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()

#Instantiate move group
group_name = "manipulator"
move_group = moveit_commander.MoveGroupCommander(group_name)

display_trajectory_publisher = rospy.Publisher("/move_group/display_planned_path", moveit_msgs.msg.DisplayTrajectory, queue_size=20,)

# Everything above is all setup stuff directly from tutorial ^^


# Define a function for moving to specific orientation and location to make path planning more succinct
# From the tutorial's Pose Plan section
def move_to_wxyz(w, x, y, z):
    pose_goal = geometry_msgs.msg.Pose()
    pose_goal.orientation.w = w
    pose_goal.position.x = x
    pose_goal.position.y = y
    pose_goal.position.z = z
    
    move_group.set_pose_target(pose_goal)

    success = move_group.go(wait=True)
    move_group.stop()
    # It is always good to clear your targets after planning with poses.
    # Note: there is no equivalent function for clear_joint_value_targets().
    move_group.clear_pose_targets()

# Define a function to get the joint values and the position of the EE
def where_am_i():
    # Get joint values
    print("Joint vals:")
    print(move_group.get_current_joint_values())
    print("")
    #Get pose
    print("Pose:")
    print(move_group.get_current_pose())
    print("")

#print robot's entire state
print("============ Printing robot state")
print(robot.get_current_state())
print("")



# Planning a joint goal - let this be the position the robot goes to first after launching Gazebo
joint_goal = move_group.get_current_joint_values()
joint_goal[0] = -pi/2
joint_goal[1] = -2*pi/3
joint_goal[2] = -2*pi/5
joint_goal[3] = 0 
joint_goal[4] = 0 
joint_goal[5] = 0 

move_group.go(joint_goal, wait=True)
move_group.stop()

# Get joint values at starting joint goal - position of interest 1
print("==========Starting config:")
where_am_i() 

# Establish starting pose
move_to_wxyz(1.0, 0.1, 0.5, 0.4)

#Start Point of J - point of interest 2
print("==========Start of J:")
where_am_i()

#==============================
## Draw J - block letter as seen below
"""
   |
|__|

"""

#First line of J, the long one on right
move_to_wxyz(1.0, 0.1, 0.5, 0.2)

# Bottom of J
move_to_wxyz(1.0, 0.1, 0.4, 0.2)

# Little left lip of J
move_to_wxyz(1.0, 0.1, 0.4, 0.3)

#================================
## Draw A -do it in block letters like so
"""
 __
|__|
|  |

"""

# Reset first
move_to_wxyz(1.0, 0.1, 0.4, 0.2)

# Start Point of A - point of interest 3
print("============Start of A:")
where_am_i()

# Left line of A
move_to_wxyz(1.0, 0.1, 0.4, 0.4)

# Top line of A
move_to_wxyz(1.0, 0.1, 0.5, 0.4)

# Right line of A
move_to_wxyz(1.0, 0.1, 0.5, 0.2)

#middle line of A, first move up then over
move_to_wxyz(1.0, 0.1, 0.5, 0.3)
move_to_wxyz(1.0, 0.1, 0.4, 0.3)

#===================================
## Draw M
"""
|\/|
|  |

"""

#Left line, reset first
move_to_wxyz(1.0, 0.1, 0.4, 0.2)
move_to_wxyz(1.0, 0.1, 0.4, 0.4)


#Middle downslash
move_to_wxyz(1.0, 0.1, 0.5, 0.3)

#middle upslash
move_to_wxyz(1.0, 0.1, 0.6, 0.4)

#Right line
move_to_wxyz(1.0, 0.1, 0.6, 0.2)