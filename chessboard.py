# Jacob Manders
# Setting up the chessboard and logic for the final project

import numpy

# Here's how we'll keep track: first digit will indicate if it's white (1) or black (2) and second will indicate the piece type
# pawn (1) rook (2) knight (3) bishop (4) king (5) queen (6)
# so example is white rook is 12, black queen is 25

#initial config - make a function for this reset?
def reset_board():
    board = numpy.zeros((8,8), dtype=int)
    board[0] = [22, 23, 24, 25, 26, 24, 23, 22]
    board[1] = [21, 21, 21, 21, 21, 21, 21, 21]

    board[6] = [11, 11, 11, 11, 11, 11, 11, 11]
    board[7] = [12, 13, 14, 15, 16, 14, 13, 12]
    return board

def is_my_piece(board, square, isWhiteTurn):
    if (board[square[1]][square[2]] == 0):
        # print("No piece in indicated position")
        return 0
    if ((board[square[1]][square[0]] % 10 == 1) != isWhiteTurn):
        # print("Piece in indicated square is not mine")
        return -1
    if ((board[square[1]][square[0]] % 10 == 1) == isWhiteTurn):
        return 1
    else:
        print("somethings broken - is_my_piece")
        return -2


# represent isWhiteTurn as boolean, start and end as 1x2 array rep. Letter, # 
def try_move(board, isWhiteTurn, start, end):
    if (is_my_piece(board, start, isWhiteTurn) == 0):
        print("No piece in inidcated position")
        return board
    if (is_my_piece(board, start, isWhiteTurn) == -1):
        print("Piece is indicated position is not yours")
        return board
    if (is_my_piece(board, start, isWhiteTurn) == -2):
        print("error: is_my_piece == -2")
        return board


board = reset_board()
print(board)
isWhiteTurn = True